import hashlib
import hmac
import scrypt

__pass_chars = {"V": "AEIOU",
             "C": "BCDFGHJKLMNPQRSTVWXYZ",
             "v": "aeiou",
             "c": "bcdfghjklmnpqrstvwxyz",
             "A": "AEIOUBCDFGHJKLMNPQRSTVWXYZ",
             "a": "AEIOUaeiouBCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz",
             "n": "0123456789",
             "o": "@&%?,=[]_:-+*$#!'^~;()/.",
             "x": "AEIOUaeiouBCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz0123456789!@#$%^&*()",
             " ": " "}

__templates = {
    "maximum": [
        "anoxxxxxxxxxxxxxxxxx",
        "axxxxxxxxxxxxxxxxxno"
    ],
    "long": [
        "CvcvnoCvcvCvcv",
        "CvcvCvcvnoCvcv",
        "CvcvCvcvCvcvno",
        "CvccnoCvcvCvcv",
        "CvccCvcvnoCvcv",
        "CvccCvcvCvcvno",
        "CvcvnoCvccCvcv",
        "CvcvCvccnoCvcv",
        "CvcvCvccCvcvno",
        "CvcvnoCvcvCvcc",
        "CvcvCvcvnoCvcc",
        "CvcvCvcvCvccno",
        "CvccnoCvccCvcv",
        "CvccCvccnoCvcv",
        "CvccCvccCvcvno",
        "CvcvnoCvccCvcc",
        "CvcvCvccnoCvcc",
        "CvcvCvccCvccno",
        "CvccnoCvcvCvcc",
        "CvccCvcvnoCvcc",
        "CvccCvcvCvccno"
    ],
    "medium": [
        "CvcnoCvc",
        "CvcCvcno"
    ],
    "basic": [
        "aaanaaan",
        "aannaaan",
        "aaannaaa"
    ],
    "short": [
        "Cvcn"
    ],
    "pin": [
        "nnnn"
    ],
    "name": [
        "cvccvcvcv"
    ],
    "phrase": [
        "cvcc cvc cvccvcv cvc",
        "cvc cvccvcvcv cvcv",
        "cv cvccv cvc cvcvccv"
    ]
}


def __int_to_bytes(num: int) -> bytes:
    return bytes(num >> (i * 8) & 0xff for i in range(3, -1, -1))


def __generate(site_key: bytes, template_type: str = "long") -> str:
    """
    Generate the password using the templates
    """
    if template_type not in __templates:
        raise ValueError('Argument template invalid')

    template = __templates[template_type][site_key[0] % len(__templates[template_type])]
    password = ""
    for i in range(len(template)):
        pass_chars = __pass_chars[template[i]]
        password += pass_chars[site_key[i + 1] % len(pass_chars)]
    return password


def get_password(name: str, password: str, site: str, template_type: str = "long", counter: int = 1,
                 _context: str = "com.lyndir.masterpassword"):
    """
    :param name: user name
    :param password: master password
    :param site: site's domain
    :param template_type:
    :param counter:
    :param _context:
    :return:
    """
    name_e = name.encode()
    password_e = password.encode()
    context = _context.encode()
    site_e = site.encode()
    salt = context + __int_to_bytes(len(name_e)) + name_e
    master_key = scrypt.hash(password_e, salt=salt, N=32768, r=8, p=2, buflen=64)
    salt = context + __int_to_bytes(len(site_e)) + site_e + __int_to_bytes(counter)
    site_key = hmac.new(master_key, salt, hashlib.sha256).digest()
    return __generate(site_key, template_type)


# ejemplo:
# psw = get_password("Pedro Pérez", "P4ssw0rd", "google.com", "long")
# print(psw)
